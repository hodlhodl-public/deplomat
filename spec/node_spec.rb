require 'spec_helper'

describe Deplomat::Node do

  before(:each) do
    @fixtures_dir = Dir.pwd + "/spec/fixtures"
    @node = Deplomat::Node.new(logfile: "#{@fixtures_dir}/deplomat.log", log_to: [:file, :stdout])
  end

  describe "logging" do

    after(:each) do
      @node.log_to = []
      @node.remove("#{@fixtures_dir}/deplomat.log")
    end

    it "logs to the stdout" do
      expect($stdout).to receive(:print).exactly(4).times
      @node.execute("ls #{@fixtures_dir}/dir1")
    end

    it "logs to a log file" do
      @node.log_to = [:file]
      @node.execute("ls #{@fixtures_dir}/dir1")
      log = File.readlines("#{@fixtures_dir}/deplomat.log")
      expect(log.size).to eq(4)
    end

    it "puts additional messages into the terminal when required" do
      expect($stdout).to receive(:print).once.with("hello\n".white)
      expect($stdout).to receive(:print).once.with("--> ls #{@fixtures_dir}/dir1\n".white)
      expect($stdout).to receive(:print).once.with("  file1\n".light_black)
      expect($stdout).to receive(:print).once.with("  file2\n".light_black)
      expect($stdout).to receive(:print).once.with("  subdir1\n".light_black)
      expect($stdout).to receive(:print).once.with("bye\n".white)
      @node.execute("ls #{@fixtures_dir}/dir1", message: ["hello", "bye"])
    end

  end

end
