require 'spec_helper'

describe Deplomat::LocalNode do

  before(:each) do
    @fixtures_dir = Dir.pwd + "/spec/fixtures"
    @localhost = Deplomat::LocalNode.new
    @localhost.log_to = []
    @localhost.raise_exceptions = false
  end

  it "checks if file exists" do
    expect(@localhost.file_exists?("#{@fixtures_dir}/existing_file.txt")).to be_falsey
    expect(@localhost.file_exists?("/non-existent-path")).to be_falsey
    FileUtils.touch("#{@fixtures_dir}/existing_file.txt")
    puts "#{@fixtures_dir}/existing_file.txt"
    expect(@localhost.file_exists?("#{@fixtures_dir}/existing_file.txt")).to be_truthy
  end

  it "changes directory correctly adding another slash" do
    @localhost.current_path = Dir.pwd
    @localhost.create_dir("tmp")
    @localhost.cd("tmp/")
    expect(@localhost.current_path).to eq("#{Dir.pwd}/tmp")
    @localhost.cd("../")
    expect(@localhost.current_path).to eq(Dir.pwd)
  end

  describe "filesystem operations" do

    it "copies files to another folder" do
      @localhost.copy("#{@fixtures_dir}/dir1/*", "#{@fixtures_dir}/dir2/")
      expect(File.exist?("#{@fixtures_dir}/dir2/file1")).to be_truthy
      expect(File.exist?("#{@fixtures_dir}/dir2/file2")).to be_truthy
    end

    it "moves files and dirs to another directory" do
      @localhost.move("#{@fixtures_dir}/dir1/*", "#{@fixtures_dir}/dir2/")
      expect(File.exist?("#{@fixtures_dir}/dir2/file1")).to be_truthy
      expect(File.exist?("#{@fixtures_dir}/dir2/file2")).to be_truthy
      expect(File.exist?("#{@fixtures_dir}/dir2/subdir1")).to be_truthy
      expect(File.exist?("#{@fixtures_dir}/dir1/file1")).to be_falsy
      expect(File.exist?("#{@fixtures_dir}/dir1/file2")).to be_falsy
      expect(File.exist?("#{@fixtures_dir}/dir1/subdir1")).to be_falsy

      @localhost.move("#{@fixtures_dir}/dir2/*", "#{@fixtures_dir}/dir1/")
      expect(File.exist?("#{@fixtures_dir}/dir1/file1")).to be_truthy
      expect(File.exist?("#{@fixtures_dir}/dir1/file2")).to be_truthy
      expect(File.exist?("#{@fixtures_dir}/dir1/subdir1")).to be_truthy
      expect(File.exist?("#{@fixtures_dir}/dir2/file1")).to be_falsy
      expect(File.exist?("#{@fixtures_dir}/dir2/file2")).to be_falsy
      expect(File.exist?("#{@fixtures_dir}/dir2/subdir1")).to be_falsy
    end

    it "removes files and dirs" do
      @localhost.copy("#{@fixtures_dir}/dir1/*", "#{@fixtures_dir}/dir2/")
      @localhost.remove("#{@fixtures_dir}/dir2/*")
      expect(File.exist?("#{@fixtures_dir}/dir2/file1")).to be_falsy
      expect(File.exist?("#{@fixtures_dir}/dir2/file2")).to be_falsy
      expect(File.exist?("#{@fixtures_dir}/dir2/subdir1")).to be_falsy
    end

    it "changes current directory" do
      @localhost.cd("/etc/")
      expect(@localhost.current_path).to eq("/etc")
      expect(-> { @localhost.cd("/non-existent-path") }).to raise_exception(Deplomat::NoSuchPathError)
    end

    it "executes commands from the current directory" do
      @localhost.cd("#{@fixtures_dir}/dir1")
      expect(@localhost.execute("ls")[:out]).to eq("file1\nfile2\nsubdir1\n")
    end

    it "creates an empty file" do
      @localhost.cd("#{@fixtures_dir}/dir2")
      @localhost.touch("myfile")
      expect(@localhost.execute("ls")[:out]).to have_files("myfile")
    end

    it "creates a directory" do
      @localhost.cd("#{@fixtures_dir}/dir2")
      @localhost.mkdir("mydir")
      expect(@localhost.execute("ls")[:out]).to have_files("mydir")
    end

    it "creates a symlink" do
      @localhost.cd("#{@fixtures_dir}/dir2")
      @localhost.create_symlink("../dir1", "./")
      expect(@localhost.execute("ls dir1")[:out]).to have_files("file1", "file2", "subdir1")
    end

  end

  describe "handling status and errors" do

    it "returns the status" do
      expect(@localhost.execute("ls #{@fixtures_dir}/dir1/")[:status]).to eq(0)
      expect(@localhost.execute("ls #{@fixtures_dir}/non-existent-dir/")[:status]).to eq(2)
    end

    it "raises execution error when command fails" do
      @localhost.raise_exceptions = true
      expect( -> { @localhost.execute("ls #{@fixtures_dir}/non-existent-dir/")[:status] }).to raise_exception(Deplomat::ExecutionError)
    end

  end

  describe "cleaning" do

    before(:each) do
      @localhost.cd("#{@fixtures_dir}/cleaning")
      @localhost.execute("touch file1 file2 file3")
      @localhost.execute("mkdir current")
      sleep(0.01)
      @localhost.execute("mkdir dir1 dir2 dir3")
    end

    after(:each) do
      @localhost.execute("rm -rf #{@fixtures_dir}/cleaning/*")
    end

    it "cleans all the files and dirs in a given directory" do
      @localhost.clean
      expect(@localhost.execute('ls')[:out]).to be_empty
    end

    it "cleans all but last 3 entries in a given directory" do
      @localhost.clean(leave: [3, :last])
      expect(@localhost.execute('ls')[:out]).to eq("dir1\ndir2\ndir3\n")
    end

    it "cleans all, but :except entries in a given directory" do
      @localhost.clean(except: ["current"])
      expect(@localhost.execute('ls')[:out]).to eq("current\n")
    end

  end

  describe "git commands" do

    before(:all) do
      @fixtures_dir  = File.expand_path(File.dirname(__FILE__)) + "/fixtures"
      @remote_node_fixtures_dir = File.expand_path(File.dirname(__FILE__)) + "/remote_node_fixtures"
      @localhost_git = Deplomat::LocalNode.new
      @localhost_git.log_to = [:stdout]
      @localhost_git.cd(@fixtures_dir)
      @localhost_git.mkdir("git_repo")
      @localhost_git.cd("git_repo")
      @localhost_git.touch("file1")
      @localhost_git.execute("git init && git add .")
      @localhost_git.execute("git remote add origin #{ENV['USER']}@localhost:#{@remote_node_fixtures_dir}/git_repo || echo \"remote exists\"")
      @remote_node = Deplomat::RemoteNode.new(host: "localhost", user: ENV['USER'])
      @remote_node.log_to = [:stdout]
      @remote_node.execute("cd #{@remote_node_fixtures_dir} && mkdir git_repo && cd git_repo && git init && git config receive.denyCurrentBranch ignore ")
      @remote_node.cd("#{@remote_node_fixtures_dir}/git_repo")
      @localhost_git.cd("#{@fixtures_dir}/git_repo")
      @localhost_git.execute("git commit -m \"Initial commit\"")

      @localhost_git.raise_exceptions = true
      @remote_node.raise_exceptions   = true
    end

    after(:all) do
      @remote_node.remove("#{@remote_node_fixtures_dir}/git_repo")
      @localhost_git.remove("#{@fixtures_dir}/git_repo")
    end

    it "pushes to remote" do
      expect(@localhost_git.git_push[:status]).to eq(0)
      expect(@remote_node.execute("git log -n1")[:out]).to include("Initial commit")
    end

    it "pulls from remote" do
      @remote_node.execute("touch commit2.txt && git add commit2.txt && git commit -m \"Commit 2\"")
      expect(@localhost_git.git_pull[:status]).to eq(0)
      expect(@localhost_git.execute("git log -n1")[:out]).to include("Commit 2")
    end

    it "merges branches" do
      @localhost_git.execute("git checkout -b dev1")
      @localhost_git.touch("file2")
      @localhost_git.execute("git add . && git commit -m \"adding file2\"")
      @localhost_git.execute("git checkout master")
      expect(@localhost_git.git_merge("dev1", "master")[:status]).to eq(0)
      expect(File.exist?("#{@fixtures_dir}/git_repo/file2")).to be_truthy
    end

    it "checks out into a branch" do
      @localhost_git.execute("git branch dev2")
      @localhost_git.touch("file3")
      @localhost_git.execute("git add . && git commit -m \"adding file3\"")
      @localhost_git.git_checkout("dev2")
      expect(File.exist?("#{@fixtures_dir}/git_repo/file3")).to be_falsy
    end

  end

end
