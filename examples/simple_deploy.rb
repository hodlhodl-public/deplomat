# This is an example of a deployment script. This file is not executable and uses a simple DSL
# which is implemented using Ruby. Thus, it may contain valid ruby code, but it's not necessary
# to know Ruby to write deployment scripts - all that's required is to know a few simple common
# deplomat directives.

# executes right away, but only in particular environment
execute_in env: "staging" do
  $db  = new_node host: "0.0.0.0", port: 1234, user: "deploy" 
  $app = new_node host: "0.0.0.0", port: 1234, user: "deploy" 
end

# doesn't execute right away and awaits until called with `execute :compile_assets` 
partial 'compile_assets' do
  $db.execute  "echo compiling assets..."
  $app.execute "echo compiling assets..."
end

partial 'run_migrations' do |args|
  args[:server].execute "rake db:migrate"
end

execute_partial 'run_migrations', node: $app
